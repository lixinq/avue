const steps = [{
    element: '.guide-container',
    popover: {
        title: 'Title on Popover',
        description: 'Body of the popover',
        position: 'left'
    }
}]

export default steps